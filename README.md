# MasterMind
Introduction to Computing Systems and Programming - UT CE - Spring 2019

this was a simple code for Master Mind game LOGIC in C language. note that this program is just for game's logic and have no ant graphical interface.

![Alt text](./readme.PNG?raw=true "Game Environment")

### Requirements :
  NOTHING.

### Contributors :
  - Behzad Shayegh
